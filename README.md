# Happy Puppy

**Authors:** Monika Šlachtová, Janka Marschalková, Zdeno Nekvasil, Patrik Mažári.

## Camel

- It is recommended to use Linux/WSL.
- Install jBang.
- Install Camel: run `jbang app install camel@apache/camel`.
- Change path to DB: can be found in `./camel-happy-puppy/application.properties` under the key `camel.beans.myDataSource.url`.
- Run Camel: In Linux/WSL `camel run integration.xml application.properties --deps=org.xerial:sqlite-jdbc:3.41.2.1 --dev`.

### Endpoints

- Note: If you are using Windows, it is necessary to change `localhost` to WSL's IP address.

#### Employee Training

- `GET http://localhost:8081/training`.
- `GET http://localhost:8081/tutorial/{trainingId}`.

#### Reservations for Dogs
- `GET http://172.20.249.134:8081/dogs/{id}`.
- `POST http://localhost:8081/dogs`.
